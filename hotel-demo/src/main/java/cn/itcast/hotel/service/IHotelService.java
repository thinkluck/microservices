package cn.itcast.hotel.service;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface IHotelService extends IService<Hotel> {
    PageResult search(RequestParams params);

    /**
     * 品牌城市...数据分组聚合
     *
     * @return
     */
    Map<String, List<String>> filters(RequestParams params);

    /**
     * 自动补全
     * @param prefix 前缀
     * @return 补全列表
     */
    List<String> listSuggestion(String prefix);

    /**
     * 根据id删除酒店索引库数据
     * @param id 酒店id
     */
    void deleteById(Long id);

    /**
     * 根据id插入或更新酒店索引库数据
     * @param id 酒店id
     */
    void insertOrUpdateById(Long id);
}
