package cn.itcast.hotel.controller;

import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import cn.itcast.hotel.service.IHotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author liutao
 * @DateTime 2022/12/22 15:42
 */
@RestController
@RequestMapping("/hotel")
public class HotelController {
    @Autowired
    private IHotelService iHotelService;

    @PostMapping("/list")
    public PageResult search(@RequestBody RequestParams params) {
        return iHotelService.search(params);
    }

    @PostMapping("/filters")
    public Map<String, List<String>> getFilters(@RequestBody RequestParams params){
        return iHotelService.filters(params);
    }

    /**
     * 自动补全返回提示词
     */
    @GetMapping("/suggestion")
    public List<String> listSuggestion(@RequestParam("key") String prefix){
        return iHotelService.listSuggestion(prefix);
    }
}
