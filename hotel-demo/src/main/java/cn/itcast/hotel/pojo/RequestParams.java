package cn.itcast.hotel.pojo;

import lombok.Data;

/**
 * @Author liutao
 * @DateTime 2022/12/22 15:38
 */
@Data
public class RequestParams {
    private String key;
    private Integer page;
    private Integer size;
    private String sortBy;
    private String city;
    private String brand;
    private String starName;
    private Integer minPrice;
    private Integer maxPrice;
    private String location;
}
