package cn.itcast.hotel.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
public class HotelDoc {
    private Long id;
    private String name;
    private String address;
    private Integer price;
    private Integer score;
    private String brand;
    private String city;
    private String starName;
    private String business;
    private String location;
    private String pic;
    private Object distance;
    private Boolean isAD;
    /**
     * 放自动补全的词条内容
     */
    private List<String> suggestion;

    public HotelDoc(Hotel hotel) {
        this.id = hotel.getId();
        this.name = hotel.getName();
        this.address = hotel.getAddress();
        this.price = hotel.getPrice();
        this.score = hotel.getScore();
        this.brand = hotel.getBrand();
        this.city = hotel.getCity();
        this.starName = hotel.getStarName();
        this.business = hotel.getBusiness();
        this.location = hotel.getLatitude() + ", " + hotel.getLongitude();
        this.pic = hotel.getPic();
        /**
         * 将品牌和商圈放进去，形成自动补全字段
         */
        ArrayList<String> suggestion = new ArrayList<>();
        suggestion.add(this.brand);
        //由于商圈可能多个以“、”或“/”分隔，所以分开逐个添加达成商圈补全
        if (this.business.contains("、")) {
            String[] split = this.business.split("、");
            Collections.addAll(suggestion, split);
        } else if (this.business.contains("/")) {
            String[] split = this.business.split("/");
            Collections.addAll(suggestion, split);
        } else {
            suggestion.add(this.business);
        }

        this.suggestion = suggestion;
    }
}
