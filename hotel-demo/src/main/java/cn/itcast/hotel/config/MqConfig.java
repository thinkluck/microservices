package cn.itcast.hotel.config;

import cn.itcast.hotel.constants.MqConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MQ配置类
 */
@Configuration
public class MqConfig {
    /**
     * 声明一个交换机
     * @return
     */
    @Bean
    public TopicExchange topicExchange(){
        //                                  交换机名称          是否持久化      是否自动删除
        return new TopicExchange(MqConstants.HOTEL_EXCHANGE,true,false);
    }

    /**
     * 声明新增/修改队列
     * @return
     */
    @Bean
    public Queue inserQueue(){
        //                          队列名称                 是否持久化
        return new Queue(MqConstants.HOTEL_INSERT_QUEUE,true);
    }
    /**
     * 声明删除队列
     * @return
     */
    @Bean
    public Queue deleteQueue(){
        //                          队列名称                 是否持久化
        return new Queue(MqConstants.HOTEL_DELETE_QUEUE,true);
    }

    /**
     * 绑定新增/修改 队列、交换机、routingKey
     * @return
     */
    @Bean
    public Binding insertQueueBinding(){
        //                        队列               交换机                        routingKey
        return BindingBuilder.bind(inserQueue()).to(topicExchange()).with(MqConstants.HOTEL_INSERT_KEY);
    }

    /**
     * 绑定删除 队列、交换机、routingKey
     * @return
     */
    @Bean
    public Binding deleteQueueBinding(){
        //                        队列               交换机                        routingKey
        return BindingBuilder.bind(deleteQueue()).to(topicExchange()).with(MqConstants.HOTEL_DELETE_KEY);
    }
}
