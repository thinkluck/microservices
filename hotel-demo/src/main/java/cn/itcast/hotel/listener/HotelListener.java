package cn.itcast.hotel.listener;

import cn.itcast.hotel.constants.MqConstants;
import cn.itcast.hotel.service.impl.HotelService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 监听酒店mq
 */
@Component
public class HotelListener {
    @Autowired
    private HotelService hotelService;

    /**
     * 监听酒店新增/修改业务
     *
     * @param id 酒店id
     */
    @RabbitListener(queues = MqConstants.HOTEL_INSERT_QUEUE)
    public void listenerInsertOrUpdate(Long id) {
        hotelService.insertOrUpdateById(id);
    }

    /**
     * 监听酒店删除业务
     *
     * @param id 酒店id
     */
    @RabbitListener(queues = MqConstants.HOTEL_DELETE_QUEUE)
    public void listenerDelete(Long id) {
        hotelService.deleteById(id);
    }
}
