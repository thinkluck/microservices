package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 酒店文档测试
 *
 * @Author liutao
 * @DateTime 2022/9/28 17:49
 */
@SpringBootTest
public class HotelDocumentTest {
    @Autowired
    private IHotelService hotelService;
    private RestHighLevelClient client;
    @Value("${config.elasticsearch.url}")
    private String clientUrl;

    /**
     * 在每个测试方法前将客户端进行初始化
     */
    @BeforeEach
    void setUp() {
        this.client = new RestHighLevelClient(RestClient.builder(HttpHost.create(clientUrl) //服务端地址
        ));
    }

    /**
     * 在每个测试方法执行后将客户端关闭
     */
    @SneakyThrows
    @AfterEach
    void afterAll() {
        this.client.close();
    }

    /**
     * 文档数据插入
     */
    @SneakyThrows
    @Test
    void addDocument() {
        //在MySQL中根据id查询酒店数据
        Hotel hotel = hotelService.getById(61083L);
        //将数据库实体hotel转换为文档类型的实体类型
        HotelDoc hotelDoc = new HotelDoc(hotel);
        //1.准备request对象
        IndexRequest request = new IndexRequest("hotel").id(hotelDoc.getId().toString());
        //将数据转换为json
        String jsonHotelDoc = JSON.toJSONString(hotelDoc);
        //2.准备JSON格式文档
        request.source(jsonHotelDoc, XContentType.JSON);
        //3.发送请求
        client.index(request, RequestOptions.DEFAULT);
    }

    /**
     * 文档数据查询
     */
    @SneakyThrows
    @Test
    void getDocumentById() {
        GetRequest request = new GetRequest("hotel").id("61083");
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        //拿到json数据
        String hotel = response.getSourceAsString();
        //转换为实体类类型
        HotelDoc hotelDoc = JSON.parseObject(hotel, HotelDoc.class);
        System.out.println(hotelDoc);
    }

    /**
     * 文档数据更新
     */
    @SneakyThrows
    @Test
    void updateDocumentById() {
        UpdateRequest request = new UpdateRequest("hotel", "61083");
        //更新数据，key,value形式
        request.doc(
                "price", "952",
                "starName", "四钻"
        );
        //3.更新文档
        client.update(request, RequestOptions.DEFAULT);
    }

    /**
     * 文档数据删除
     */
    @SneakyThrows
    @Test
    void deleteDocumentById() {
        DeleteRequest request = new DeleteRequest("hotel", "61083");
        client.delete(request, RequestOptions.DEFAULT);
    }

    /**
     * 批量新增文档数据
     */
    @SneakyThrows
    @Test
    void batchAddDocument() {
        //查询所有酒店数据
        List<Hotel> hotels = hotelService.list();
        //批量操作器
        BulkRequest request = new BulkRequest();
        //遍历hotel数据
        for (Hotel hotel : hotels) {
            //将mysql实体转换为hotelDoc实体
            HotelDoc hotelDoc = new HotelDoc(hotel);
            //往里一个一个加操作
            request.add(
                    new IndexRequest("hotel")
                            .id(hotelDoc.getId().toString())
                            .source(JSON.toJSONString(hotelDoc), XContentType.JSON)
            );
        }
        //执行批处理
        client.bulk(request, RequestOptions.DEFAULT);
    }
}

