package cn.itcast.hotel;

import lombok.SneakyThrows;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 酒店索引测试
 *
 * @Author liutao
 * @DateTime 2022/9/28 17:49
 */
@SpringBootTest
public class HotelSearchTest {
    private RestHighLevelClient client;
    @Value("${config.elasticsearch.url}")
    private String clientUrl;

    /**
     * 在每个测试方法前将客户端进行初始化
     */
    @BeforeEach
    void setUp() {
        this.client = new RestHighLevelClient(RestClient.builder(HttpHost.create(clientUrl) //服务端地址
        ));
    }

    /**
     * 在每个测试方法执行后将客户端关闭
     */
    @SneakyThrows
    @AfterEach
    void afterAll() {
        this.client.close();
    }

    /**
     * 检索所有数据
     */
    @SneakyThrows
    @Test
    void MatchAll() {
        SearchRequest request = new SearchRequest("hotel");
        //准备DSL
        request.source().query(QueryBuilders.matchAllQuery());
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //已经拿到整个数据了，再进行逐层解析json，拿到想要的结果
        parseResponse(response);
    }

    /**
     * 检索指定数据
     * 都可以利用QueryBuilders对应DSL语句进行检索
     */
    @SneakyThrows
    @Test
    void Match() {
        SearchRequest request = new SearchRequest("hotel");
        //准备DSL
        //单字段搜索                                         对应字段       搜索内容
        request.source().query(QueryBuilders.matchQuery("name", "如家"));
        //多字段搜索                                             搜索内容           对应字段    对应字段
        request.source().query(QueryBuilders.multiMatchQuery("如家", "name", "business"));
        //添加条件搜索
        request.source().query(QueryBuilders.boolQuery().must(QueryBuilders.termQuery("city", "上海")));
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //已经拿到整个数据了，再进行逐层解析json，拿到想要的结果
        parseResponse(response);
    }

    /**
     * 检索数据分页排序
     * 都可以利用QueryBuilders对应DSL语句进行分页排序
     */
    @SneakyThrows
    @Test
    void pageAndSort() {
        SearchRequest request = new SearchRequest("hotel");

        //准备DSL
        //查询所有
        request.source().query(QueryBuilders.matchAllQuery());
        //排序
        request.source().sort("price", SortOrder.ASC);
        //分页
        request.source().from(0).size(5);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);

        parseResponse(response);
    }

    /**
     * 解析响应
     *
     * @param response
     */
    private static void parseResponse(SearchResponse response) {
        //已经拿到整个数据了，再进行逐层解析json，拿到想要的结果
        SearchHit[] hits = response.getHits().getHits();
        //取出检索的所有数据
        for (SearchHit hit : hits) {
            String source = hit.getSourceAsString();
            System.out.println(source);
        }
    }

    /**
     * 检索数据对指定字段进行高亮
     * 都可以利用QueryBuilders对应DSL语句进行高亮
     */
    @SneakyThrows
    @Test
    void highlight() {
        SearchRequest request = new SearchRequest("hotel");

        //准备DSL
        //查询                                        对all字段     查询 如家
        request.source().query(QueryBuilders.matchQuery("all", "如家"));
        //对name高亮                                             高亮字段             不强制高亮字段与检索字段相同
        request.source().highlighter(new HighlightBuilder().field("name").requireFieldMatch(false));
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);

        //已经拿到整个数据了，再进行逐层解析json，拿到想要的结果
        SearchHit[] hits = response.getHits().getHits();
        //取出检索的所有数据(高亮的)
        for (SearchHit hit : hits) {
            Text[] names = hit.getHighlightFields().get("name").getFragments();
            for (Text name : names) {
                System.out.println(name);
            }
        }
    }

    /**
     * 对数据聚合分组
     */
    @Test
    void testAggregation() throws IOException {
        //1.准备request
        SearchRequest request = new SearchRequest("hotel");
        //2.准备DSL
        //2.1设置size（即过滤后的完整文档数据）
        request.source().size(0);
        //2.2聚合分组                    聚合工具类                  聚合名称(自定义)    聚合字段       聚合条数
        request.source().aggregation(AggregationBuilders.terms("brandAgg").field("brand").size(20));
        //3.发起请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //4.解析结果 response已经是结果了，只需要根据json逐层解析
        Aggregations aggregations = response.getAggregations();
        Terms brandAgg = aggregations.get("brandAgg");
        List<? extends Terms.Bucket> buckets = brandAgg.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            System.out.println(bucket.getKey());
        }
    }

    /**
     * 自动补全查询
     */
    @SneakyThrows
    @Test
    public void testSuggest() {
        //1.准备Request                             索引库名称
        SearchRequest request = new SearchRequest("hotel");
        //2.准备DSL
        request.source().suggest(new SuggestBuilder().addSuggestion(
                "suggestion", //字段
                SuggestBuilders.completionSuggestion("suggestion")//补全字段
                        .prefix("h")//查询条件输入
                        .skipDuplicates(true)//跳过重复
                        .size(10)//十条
        ));
        //3.发起请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //4.结果解析(逐层解析结果json)
        ArrayList<String> suggests = new ArrayList<>();
        for (Suggest.Suggestion.Entry.Option option : response.getSuggest().getSuggestion("suggestion").getEntries().get(0).getOptions()) {
            suggests.add(String.valueOf(option.getText()));
        }
        System.out.println(suggests);
    }
}

