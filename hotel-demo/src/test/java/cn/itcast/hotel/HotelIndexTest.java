package cn.itcast.hotel;

import cn.itcast.hotel.constants.HotelConstants;
import lombok.SneakyThrows;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 酒店索引测试
 *
 * @Author liutao
 * @DateTime 2022/9/28 17:49
 */
@SpringBootTest
public class HotelIndexTest {
    private RestHighLevelClient client;
    @Value("${config.elasticsearch.url}")
    private String clientUrl;

    /**
     * 在每个测试方法前将客户端进行初始化
     */
    @BeforeEach
    void setUp() {
        this.client = new RestHighLevelClient(RestClient.builder(HttpHost.create(clientUrl) //服务端地址
        ));
    }

    /**
     * 在每个测试方法执行后将客户端关闭
     */
    @SneakyThrows
    @AfterEach
    void afterAll() {
        this.client.close();
    }

    /**
     * 测试
     */
    @Test
    void testInit() {
        System.out.println(client);
    }

    /**
     * 创建索引库
     */
    @SneakyThrows
    @Test
    void createHotelIndex() {
        //1.创建request对象                                   索引名（库名）
        CreateIndexRequest request = new CreateIndexRequest("hotel");
        //2.准备请求参数        创建索引的结构语句              JSON格式
        request.source(HotelConstants.MAPPING_TEMPLATE, XContentType.JSON);
        //3.发送请求       创建    request对象       默认
        client.indices().create(request, RequestOptions.DEFAULT);
    }

    /**
     * 判断索引库是否存在
     */
    @SneakyThrows
    @Test
    void existsHotelIndex() {
        //创建request对象                                   索引名（库名）
        GetIndexRequest request = new GetIndexRequest("hotel");
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    /**
     * 删除索引库
     */
    @SneakyThrows
    @Test
    void deleteHotelIndex() {
        //创建request对象                                   索引名（库名）
        DeleteIndexRequest request = new DeleteIndexRequest("hotel");
        client.indices().delete(request, RequestOptions.DEFAULT);
    }
}

