package cn.itcast.mq.spring;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAmqpTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * ConfirmCallBack的例子
     * 发送到交换机失败时ACK
     *
     * @throws InterruptedException
     */
    @Test
    public void testSendMessage2SimpleQueue() throws InterruptedException {
        String routingKey = "simple.test";
        String message = "hello, spring amqp!";

        //ConfirmCallBack实现
        //1.指定消息ID，避免ACK ID重复
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        //2.添加ConfirmCallBack
        correlationData.getFuture().addCallback(
                new SuccessCallback<CorrelationData.Confirm>() {
                    @Override
                    public void onSuccess(CorrelationData.Confirm confirm) {
                        //ack
                        if (confirm.isAck()) {
                            log.debug("消息投递到交换机成功！ID{}", correlationData.getId());
                            //nack
                        } else {
                            log.error("消息投递到交换机失败！ID:{},原因{}", correlationData.getId(), confirm.getReason());
                        }
                    }
                },
                new FailureCallback() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        log.error("消息投递到交换机异常，ID{}，原因{}", correlationData.getId(), throwable.getMessage());
                    }
                }
        );

        //3.发送消息时指定correlationData
        rabbitTemplate.convertAndSend("amq.topic", routingKey, message, correlationData);
        //这一句可以删掉，这里为避免太快测试方法直接结束，导致还没有Callback
        Thread.sleep(2000);
    }

    /**
     * 发送持久化消息
     */
    @Test
    public void testDurableMessage() {
        //1.准备消息
        Message message = MessageBuilder
                //消息体
                .withBody("hello world".getBytes(StandardCharsets.UTF_8))
                //持久化模式
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                //构建消息
                .build();
        //2.发送消息
        rabbitTemplate.convertAndSend("simple.queue",message);
    }

    /**
     * 发送延迟消息(根据队列的延迟时间)
     */
    @Test
    public void testTTLMessage() {
        String data ="hello ttl message "+ new Date();
        //1.准备消息
        Message message = MessageBuilder
                //消息体
                .withBody(data.getBytes(StandardCharsets.UTF_8))
                //持久化模式
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                //构建消息
                .build();
        //2.发送消息
        rabbitTemplate.convertAndSend("ttl.direct","ttl",message);
    }

    /**
     * 发送延迟消息(根据消息的延迟时间)
     * 当队列和消息都设置超时时间后，会以较短的为准投递消息，二者不冲突
     */
    @Test
    public void testTTLMessageByMsg() {
        String data ="hello ttl message "+ new Date();
        //1.准备消息
        Message message = MessageBuilder
                //消息体
                .withBody(data.getBytes(StandardCharsets.UTF_8))
                //持久化模式
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                //设置消息的延迟时间
                .setExpiration("5000")
                //构建消息
                .build();
        //2.发送消息
        rabbitTemplate.convertAndSend("ttl.direct","ttl",message);
    }

    /**
     * 发送延迟消息(延迟交换机插件形式)
     */
    @Test
    public void testDelayMessageByMsg() throws InterruptedException {
        String data = "hello delay message " + new Date();
        //1.准备消息
        Message message = MessageBuilder
                //消息体
                .withBody(data.getBytes(StandardCharsets.UTF_8))
                //持久化模式
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                //设置消息的延迟时间
                .setHeader("x-delay", 5000)
                //构建消息
                .build();
        //2.准备消息唯一ID
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        //3.发送消息
        rabbitTemplate.convertAndSend("delay.direct", "delay", message, correlationData);
        //这一句可以删掉，这里为避免太快测试方法直接结束，导致还没有Callback
        Thread.sleep(5000);
    }

    /**
     * 惰性队列测试
     */
    @Test
    public void testLazyMessageByMsg(){
        for (int i = 0; i < 1000000; i++) {
            String data = "hello lazy queue ";
            //1.准备消息
            Message message = MessageBuilder
                    //消息体
                    .withBody(data.getBytes(StandardCharsets.UTF_8))
                    //非持久化模式，方便查看惰性队列和普通队列的消息效果
                    .setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT)
                    //构建消息
                    .build();
            //2.发送消息
            rabbitTemplate.convertAndSend("lazy.queue", message);
        }
    }

    /**
     * 普通队列测试
     */
    @Test
    public void testNormalMessageByMsg(){
        for (int i = 0; i < 1000000; i++) {
            String data = "hello normal queue ";
            //1.准备消息
            Message message = MessageBuilder
                    //消息体
                    .withBody(data.getBytes(StandardCharsets.UTF_8))
                    //非持久化模式，方便查看惰性队列和普通队列的消息效果
                    .setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT)
                    //构建消息
                    .build();
            //2.发送消息
            rabbitTemplate.convertAndSend("normal.queue", message);
        }
    }
}
