package cn.itcast.mq.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.sound.midi.Soundbank;

@Component
@Slf4j
public class SpringRabbitListener {

    @RabbitListener(queues = "simple.queue")
    public void listenSimpleQueue(String msg) {
        log.debug("消费者接收到simple.queue的消息：【" + msg + "】");
        System.out.println(1/0);
        log.info("消费者处理消息成功！");
    }

    /**
     * 监听指定的队列
     * 这里是监听定义的死信队列
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("dl.queue"),
            exchange = @Exchange("dl.direct"),
            key ="dl"
    ))
    public void listenDlQueue(String msg) {
        log.info("消费者收到了dl.queue延迟消息："+msg);
    }

    /**
     * 插件延迟交换机形式声明监听
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("delay.queue"),
            exchange = @Exchange(value = "delay.direct", delayed = "true"),
            key = "delay"
    ))
    public void listenDelayExchange(String msg) {
        log.info("消费者收到了delay延迟交换机消息：" + msg);
    }
}
