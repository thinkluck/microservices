package cn.itcast.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 延迟消息队列
 * 流程：
 * 1.发送消息到交换机
 * 2.路由到延迟队列
 * 3.消息到达指定时间后自动发送到绑定的死信交换机
 * 4.监听死信队列
 */
@Configuration
public class TTLMessageConfig {
    /**
     * 声明延迟交换机
     * @return
     */
    @Bean
    public DirectExchange ttlDirectExchange() {
        return new DirectExchange("ttl.direct");
    }

    /**
     * 声明延迟队列
     * @return
     */
    @Bean
    public Queue ttlQueue() {
        return QueueBuilder
                .durable("ttl.queue")//交换机名称
                .ttl(10000)//超时时间
                .deadLetterExchange("dl.direct")//指定死信交换机
                .deadLetterRoutingKey("dl")//指定死信路由key
                .build();
    }

    /**
     * 绑定延迟队列和延迟交换机，指定路由key
     * @return
     */
    @Bean
    public Binding ttlBinding() {
        return BindingBuilder.bind(ttlQueue()).to(ttlDirectExchange()).with("ttl");
    }

}
