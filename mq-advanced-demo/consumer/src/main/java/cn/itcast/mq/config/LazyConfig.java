package cn.itcast.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LazyConfig {
    /**
     * 声明惰性队列
     * @return
     */
    @Bean
    public Queue lazyQueue(){
        return QueueBuilder
                //持久化队列(队列名称)
                .durable("lazy.queue")
                //惰性队列
                .lazy()
                .build();
    }

    /**
     * 为方便测试，声明普通队列
     * @return
     */
    @Bean
    public Queue normalQueue(){
        return QueueBuilder
                //持久化队列(队列名称)
                .durable("normal.queue")
                .build();
    }
}
