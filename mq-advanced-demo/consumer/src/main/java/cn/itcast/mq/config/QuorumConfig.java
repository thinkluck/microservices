package cn.itcast.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 仲裁队列
 */
@Configuration
public class QuorumConfig {
    /**
     * 声明仲裁队列
     * @return
     */
    @Bean
    public Queue quorumQueue(){
        return QueueBuilder
                //持久化队列(队列名称)
                .durable("quorum.queue2")
                //仲裁队列
                .quorum()
                .build();
    }
}
