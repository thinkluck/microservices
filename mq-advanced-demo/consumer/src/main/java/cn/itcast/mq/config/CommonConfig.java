package cn.itcast.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonConfig {
    /**
     * 声明持久化交换机
     * @return
     */
    @Bean
    public DirectExchange simpleExchange(){
        //                               交换机名称           是否持久化     是否自动删除
        return new DirectExchange("simple.direct",true,false);
    }

    /**
     * 声明持久化队列
     * @return
     */
    @Bean
    public  Queue simpleQueue(){
        //                 声明一个持久化队列为simple.queue
        return QueueBuilder.durable("simple.queue").build();
    }
}
