package cn.itcast.account.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * seata的TCC实现用户金额扣减
 * @LocalTCC 标识为seata TCC模式
 */
@LocalTCC
public interface AccountTCCService {
    /**
     * try逻辑
     * 尝试扣除金额并预留
     * @param userId 用户id
     * @param money 金额
     * @TwoPhaseBusinessAction 标识try方法 @name 标识try方法名称 @commitMethod 标识confirm方法名称 @rollbackMethod 标识cancel方法名称
     * @BusinessActionContextParameter 参数传递到上下文 传递的参数在confirm方法和cancel方法中可以用BusinessActionContext来接收
     */
    @TwoPhaseBusinessAction(name="tryDeduct",commitMethod = "confirm",rollbackMethod = "cancel")
    void tryDeduct(@BusinessActionContextParameter(paramName = "userId") String userId,@BusinessActionContextParameter(paramName = "money") int money);

    /**
     * confirm步骤
     * @param context 上下文
     * @return
     */
    boolean confirm(BusinessActionContext context);

    /**
     * cancel步骤
     * @param context 上下文
     * @return
     */
    boolean cancel(BusinessActionContext context);
}
