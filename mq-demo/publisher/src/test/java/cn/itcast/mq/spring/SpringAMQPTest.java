package cn.itcast.mq.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author liutao
 * @DateTime 2022/8/15 14:43
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAMQPTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSendMessage() {
        String queueName = "simple.queue";
        String message = "hello springAMQP";
        //调用api发送message
        rabbitTemplate.convertAndSend(queueName, message);
    }

    /**
     * 向队列发送
     */
    @Test
    public void testWorkQueueSendMessage() {
        String queueName = "simple.queue";
        String message = "hello springAMQP__";
        for (int i = 0; i < 50; i++) {
            rabbitTemplate.convertAndSend(queueName, message + i);
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 向交换机发送
     */
    @Test
    public void testFanoutSendMessage() {
        String exchangeName = "itcast.fanout";
        String message = "hello springAMQP__";
        rabbitTemplate.convertAndSend(exchangeName, "", message);
    }

    /**
     * 向Direct交换机发送
     */
    @Test
    public void testDirectSendMessage() {
        String exchangeName = "itcast.direct"; //交换机名称
        String key = "blue"; //指定目标队列的key
        String message = "hello," + key; //发送信息
        rabbitTemplate.convertAndSend(exchangeName, key, message);
    }

    /**
     * 向Topic交换机发送
     */
    @Test
    public void testTopicSendMessage() {
        String exchangeName = "itcast.topic"; //交换机名称
        String key = "china.news"; //指定目标队列的key
        String message = "hello,TopicMQ" + key; //发送信息
        rabbitTemplate.convertAndSend(exchangeName, key, message);
    }

    /**
     * 发送map类型消息到object队列
     */
    @Test
    public void testSendObjectQueue() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "刘德华");
        map.put("age", 52);
        rabbitTemplate.convertAndSend("object.queue", map);
    }

}
