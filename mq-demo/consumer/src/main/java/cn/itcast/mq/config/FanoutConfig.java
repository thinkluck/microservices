package cn.itcast.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 声明交换机和队列，将交换机和队列进行绑定
 * Fanout类型交换机
 *
 * @Author liutao
 * @DateTime 2022/8/15 15:52
 */
@Configuration
public class FanoutConfig {
    /**
     * 声明交换机
     *
     * @return 交换机
     * @FanoutExchange 交换机类型
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("itcast.fanout");
    }

    /**
     * 声明队列
     *
     * @return 队列
     */
    @Bean
    public Queue fanoutQueue1() {
        return new Queue("fanout.queue1");
    }

    /**
     * 绑定队列与交换机
     * 方法名不能写错，做为唯一ID
     *
     * @param fanoutExchange 交换机
     * @param fanoutQueue1   队列
     * @return
     */
    @Bean
    public Binding fanoutBinding1(FanoutExchange fanoutExchange, Queue fanoutQueue1) {
        return BindingBuilder.bind(fanoutQueue1).to(fanoutExchange);
    }

    /**
     * 声明队列
     *
     * @return 队列
     */
    @Bean
    public Queue fanoutQueue2() {
        return new Queue("fanout.queue2");
    }

    /**
     * 绑定队列与交换机
     * 方法名不能写错，做为唯一ID
     *
     * @param fanoutExchange 交换机
     * @param fanoutQueue2   队列
     * @return
     */
    @Bean
    public Binding fanoutBinding2(FanoutExchange fanoutExchange, Queue fanoutQueue2) {
        return BindingBuilder.bind(fanoutQueue2).to(fanoutExchange);
    }

    /**
     * 声明一个object队列
     * @return
     */
    @Bean
    public Queue objectQueue() {
        return new Queue("object.queue");
    }

}
