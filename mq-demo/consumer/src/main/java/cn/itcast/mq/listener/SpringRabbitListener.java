package cn.itcast.mq.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.Map;

/**
 * @Author liutao
 * @DateTime 2022/8/15 14:59
 */
@Component
public class SpringRabbitListener {
    /*@RabbitListener(queues = "simple.queue")
    public void listenerSimpleQueue(String message) {
        System.out.println("消费者接收到simple.queue队列的消息：" + message);
    }*/

    @RabbitListener(queues = "simple.queue")
    public void listenerSimpleQueue1(String message) {
        System.out.println("消费者1接收到simple.queue队列的消息：【" + message + "】" + LocalTime.now());
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @RabbitListener(queues = "simple.queue")
    public void listenerSimpleQueue2(String message) {
        System.err.println("消费者2接收到simple.queue队列的消息：【" + message + "】" + LocalTime.now());
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @RabbitListener(queues = "fanout.queue1")
    public void listenerFanoutQueue1(String message) {
        System.out.println("消费者接收到fanout.queue1队列的消息：" + message);
    }

    @RabbitListener(queues = "fanout.queue2")
    public void listenerFanoutQueue2(String message) {
        System.out.println("消费者接收到fanout.queue2队列的消息：" + message);
    }

    /**
     * 利用注解一步同时注册
     * 监听 @RabbitListener
     * 绑定队列 @QueueBinding
     * 队列 @Queue
     * 交换机 @Exchange
     * 监听的key
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue1"),
            exchange = @Exchange(name = "itcast.direct", type = ExchangeTypes.DIRECT),
            key = {"red", "blue"}
    )
    )
    public void listenerDirectQueue1(String message) {
        System.out.println("消费者接收到direct.queue1队列的消息：" + message);
    }

    /**
     * 利用注解一步同时注册
     * 监听 @RabbitListener
     * 绑定队列 @QueueBinding
     * 队列 @Queue
     * 交换机 @Exchange
     * 监听的key
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue2"),
            exchange = @Exchange(name = "itcast.direct", type = ExchangeTypes.DIRECT),
            key = {"red", "yellow"}
    )
    )
    public void listenerDirectQueue2(String message) {
        System.out.println("消费者接收到direct.queue2队列的消息：" + message);
    }

    /**
     * 利用注解一步同时注册
     * 监听 @RabbitListener
     * 绑定队列 @QueueBinding
     * 队列 @Queue
     * 交换机 @Exchange
     * 监听的key
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue1"),
            exchange = @Exchange(name = "itcast.topic", type = ExchangeTypes.TOPIC),
            key = "china.#"
    ))
    public void listenerTopicQueue1(String message) {
        System.out.println("消费者接收到Topic.queue1队列的消息：" + message);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue2"),
            exchange = @Exchange(name = "itcast.topic", type = ExchangeTypes.TOPIC),
            key = "#.news"
    ))
    public void listenerTopicQueue2(String message) {
        System.out.println("消费者接收到Topic.queue2队列的消息：" + message);
    }

    @RabbitListener(queues = "object.queue")
    public void listenObjectQueue(Map<String, Object> msg) {
        System.out.println("接收到objectQueue的消息"+msg);
    }
}
