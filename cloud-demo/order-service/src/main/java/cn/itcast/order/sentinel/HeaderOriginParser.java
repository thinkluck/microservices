package cn.itcast.order.sentinel;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 自定义请求的Origin 作为sentinel授权规则流控的“流控应用”
 */
@Component
public class HeaderOriginParser implements RequestOriginParser{
    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        //自定义请求所属的“流控应用”
        //自定义：如果请求头不包含origin，就将请求归为“blank”流控应用，如果请求头包含origin，就将origin作为所属的流控应用
        String origin = httpServletRequest.getHeader("origin");
        if (StringUtils.isEmpty(origin)){
            origin="blank";
        }
        return origin;
    }
}
