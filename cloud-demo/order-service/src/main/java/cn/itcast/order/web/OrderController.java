package cn.itcast.order.web;

import cn.itcast.order.pojo.Order;
import cn.itcast.order.service.OrderService;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("order")
public class OrderController {

   @Autowired
   private OrderService orderService;

    @SentinelResource("orderHot")
    @GetMapping("{orderId}")
    public Order queryOrderByUserId(@PathVariable("orderId") Long orderId) {
        // 根据id查询订单并返回
        return orderService.queryOrderById(orderId);
    }

    /**
     * 测试关联模式限流效果
     * @return
     */
    @GetMapping("/query")
    public String queryOrder(){
        return "查询订单成功！";
    }

    /**
     * 测试关联模式限流效果
     * @return
     */
    @GetMapping("/update")
    public String updateOrder(){
        return "更新订单成功！";
    }

    /**
     * 测试链路模式限流效果
     * @return
     */
    @GetMapping("/queryOrder")
    public String queryOrder2(){
        orderService.queryOrder();
        return "查询订单成功！";
    }

    /**
     * 测试链路模式限流效果
     * @return
     */
    @GetMapping("/addOrder")
    public String addOrder(){
        orderService.queryOrder();
        return "新增订单成功！";
    }
}
