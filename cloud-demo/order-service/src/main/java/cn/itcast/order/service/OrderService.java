package cn.itcast.order.service;

import cn.itcast.feign.clients.UserClient;
import cn.itcast.feign.pojo.User;
import cn.itcast.order.mapper.OrderMapper;
import cn.itcast.order.pojo.Order;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    /* @Autowired
     private RestTemplate restTemplate;*/
    @Autowired
    private OrderMapper orderMapper;
 /*利用RestTemplate远程调用
    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        //2.利用RestTemplate发起请求查询用户信息
        String url = "http://userservice/user/" + order.getUserId();
        User user = restTemplate.getForObject(url, User.class);
        order.setUser(user);
        // 4.返回
        return order;
    }*/

    @Autowired
    private UserClient userClient;

    /**
     * 利用feign进行远程调用
     * @param orderId
     * @return
     */
    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        //2.利用feign远程调用
        User user = userClient.findById(order.getUserId());
        order.setUser(user);
        // 4.返回
        return order;
    }

    /**
     * 模拟链路模式限流
     * @SentinelResource 默认Sentinel是监听所有端点，而不会监听service，所以用注解添加这个资源到Sentinel
     * @return
     */
    @SentinelResource("queryOrderServer")
    public void queryOrder(){
        System.err.println("查询到商品！");
    }
}
