package cn.itcast.feign.config;

import cn.itcast.feign.clients.fallback.UserClientFallbackFactory;
import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * @Author liutao
 * @DateTime 2022/7/22 14:26
 */
public class DefaultFeignConfiguration {
    /**
     * 指定feign日志级别
     *
     * @return
     */
    @Bean
    public Logger.Level logLevel() {
        return Logger.Level.BASIC;
    }

    /**
     * 将调用异常的处理工厂注册为bean
     */
    @Bean
    public UserClientFallbackFactory userClientFallbackFactory(){
        return new UserClientFallbackFactory();
    }
}
