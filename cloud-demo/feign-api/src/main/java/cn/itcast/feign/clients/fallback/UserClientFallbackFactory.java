package cn.itcast.feign.clients.fallback;

import cn.itcast.feign.clients.UserClient;
import cn.itcast.feign.pojo.User;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 对feign远程调用失败的异常处理
 */
@Slf4j
public class UserClientFallbackFactory implements FallbackFactory<UserClient> {

    /**
     * 对userClient调用失败的异常处理
     *
     * @param throwable 异常
     * @return
     */
    @Override
    public UserClient create(Throwable throwable) {
        return new UserClient() {
            /**
             * findById的处理逻辑
             * @param id
             * @return
             */
            @Override
            public User findById(long id) {
                log.error("查询用户异常", throwable);
                return new User();
            }
        };
    }
}
