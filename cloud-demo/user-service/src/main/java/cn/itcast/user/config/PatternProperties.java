package cn.itcast.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 属性注入
 * @Author liutao
 * @DateTime 2022/7/19 16:54
 * 读取nacos配置中的属性 注入配置文件中的 pattern.dateformat属性
 */
@Data
@Component
@ConfigurationProperties(prefix = "pattern")
public class PatternProperties {
    private String dateformat;
    private String envSharedValue;
    private String name;
}
