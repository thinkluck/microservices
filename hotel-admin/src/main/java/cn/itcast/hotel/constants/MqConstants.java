package cn.itcast.hotel.constants;

/**
 * MQ常量类
 */
public class MqConstants {
    /**
     * 交换机
     */
    public final static String HOTEL_EXCHANGE = "hotel.topic";
    //因为es新增和修改都是根据id判断的，没有id则新增，有则修改，所以用一个队列就行
    /**
     * 新增/修改队列
     */
    public final static String HOTEL_INSERT_QUEUE = "hotel.insert.queue";
    /**
     * 删除队列
     */
    public final static String HOTEL_DELETE_QUEUE = "hotel.delete.queue";
    /**
     * 新增/修改RoutingKey
     */
    public final static String HOTEL_INSERT_KEY = "hotel.insert";
    /**
     * 删除RoutingKey
     */
    public final static String HOTEL_DELETE_KEY = "hotel.delete";
}
