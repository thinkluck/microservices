package com.heima.item.canal;

import com.github.benmanes.caffeine.cache.Cache;
import com.heima.item.config.RedisHandler;
import com.heima.item.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.annotation.CanalTable;
import top.javatool.canal.client.handler.EntryHandler;


/**
 * canal监听器
 * 实现EntryHandler即可
 *
 * @CanalTable 监听的表
 */
@CanalTable("tb_item")
@Component
public class ItemHandler implements EntryHandler<Item> {
    /**
     * 当数据库发生变化时通知canal进行redis操作
     */
    @Autowired
    private RedisHandler redisHandler;
    /**
     * 当数据库发生变化时通知canal进行caffeine操作
     */
    @Autowired
    private Cache<Long, Item> itemCache;

    /**
     * 插入时的监听
     *
     * @param item 插入的数据
     */
    @Override
    public void insert(Item item) {
        itemCache.put(item.getId(), item);
        redisHandler.saveItem(item);
    }

    /**
     * 更新时的监听
     *
     * @param before 更新前的数据
     * @param after  更新后的数据
     */
    @Override
    public void update(Item before, Item after) {
        itemCache.put(after.getId(), after);
        redisHandler.saveItem(after);
    }

    /**
     * 删除时的监听
     *
     * @param item 删除的数据
     */
    @Override
    public void delete(Item item) {
        itemCache.invalidate(item.getId());
        redisHandler.delItem(item.getId());
    }
}
