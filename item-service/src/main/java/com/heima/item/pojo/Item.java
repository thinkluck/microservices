package com.heima.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Data
@TableName("tb_item")
public class Item {
    @TableId(type = IdType.AUTO)
    @Id //标识主键字段，方便canal做监听的实体转换
    private Long id;//商品id
    @Column(name = "name") //当数据库表中列名对应不一致时使用 ，方便canal做监听的实体转换
    private String name;//商品名称
    @Column(name = "title")
    private String title;//商品标题
    @Column(name = "price")
    private Long price;//价格（分）
    @Column(name = "image")
    private String image;//商品图片
    @Column(name = "category")
    private String category;//分类名称
    @Column(name = "brand")
    private String brand;//品牌名称
    @Column(name = "spec")
    private String spec;//规格
    @Column(name = "status")
    private Integer status;//商品状态 1-正常，2-下架
    @Column(name = "create_time")
    private Date createTime;//创建时间
    @Column(name = "update_time")
    private Date updateTime;//更新时间
    @TableField(exist = false)
    @Transient //标记为不属于数据库表中的注解，方便canal做监听的实体转换
    private Integer stock;
    @TableField(exist = false)
    @Transient //标记为不属于数据库表中的注解，方便canal做监听的实体转换
    private Integer sold;
}
